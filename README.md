# `tvu-compare`

[Blog post.](https://expandingman.gitlab.io/tvu-compare/)

In this repository I write a simple command-line utility that I need for managing my TV (to which I
have a computer running Manjaro permanently attached).  Essentially this is for some minor
functionality for launching applications and controlling `i3` which is not quite achievable with
`i3` alone without (at least) an (unacceptably) elaborate shell script.

As recently I have been using [Julia](https://julialang.org) almost exclusively but, sadly, Julia is
wildly inappropriate for this kind of application (a situation which hopefully will not persist
forever), I have decided to use this as an opportunity to explore some languages I have been
interested in which can output small stand-alone binaries running highly efficient programs with
little or no run-time.

I have therefore decided to use this as an opportunity to compare [Rust](https://rust-lang.org") and
[Zig](https://ziglang.org) and to decide which I prefer to use by default.

### Requirements
The program I'm writing needs to achieve the following:
- Read a (preferrably `toml`) configuration file. This will be done on every execution to avoid the
    need for a system-wide mutable state apart from the config file itself.
- Parse command line arguments.  Ultimately I will likely need subcommands and flags, though in
    these initial implementations I may only include a very minimal CLI.
- Launch applications in a specific `i3` workspace or, if the program is already running, switch to
    the workspace.

This may not fully describe everything I want the program to do, but it's all I will require of
these trial implementations.

