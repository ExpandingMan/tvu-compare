use std::process::Command;
use std::iter::Iterator;

// requiring ToString may not be the best bounds
pub fn command_from_strings<I: Iterator>(mut v: I) -> Command where I::Item: ToString {
    let mut cmd = Command::new(v.next().expect("tried to create command from empty array").to_string());
    for a in v { cmd.arg(a.to_string()); }
    cmd
}

pub fn command_from_string(s: String) -> Command { command_from_strings(s.split_whitespace()) }

