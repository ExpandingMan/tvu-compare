const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    const exe = b.addExecutable("tvu", "src/main.zig");

    const tests = b.addTest("src/main.zig");

    //NOTE: zig-toml needs merged PR, in meantime my branch works
    // https://github.com/ExpandingMan/zig-toml/tree/emfixes

    //TODO: this is not in a `for` loop because of a compiler bug, change it back after fix
    const tomlpath = "lib/zig-toml/src/toml.zig";
    exe.addPackagePath("toml", tomlpath);
    tests.addPackagePath("toml", tomlpath);

    const test_step = b.step("test", "run tests");
    test_step.dependOn(&tests.step);

    exe.install();
}
