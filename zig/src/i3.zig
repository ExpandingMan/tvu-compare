const std = @import("std");
const json = std.json;
const log = std.log;
const fs = std.fs;
const net = std.net;
const mem = std.mem;
const Stream = net.Stream;

const I3_MAGIC_BYTES = "i3-ipc";

// need this to be global so it doesn't get freed
const I3_GETSOCKET_CMD = [_][]const u8{ "i3", "--get-socketpath" };

const I3Error = error{
    InvalidResponsePayload,
};

const Rect = struct {
    x: i64,
    y: i64,
    width: i64,
    height: i64,
};

const Workspace = struct {
    id: i64,
    num: i64,
    name: []const u8,
    visible: bool,
    focused: bool,
    rect: Rect,
    output: []const u8,
    urgent: bool,
};

pub const I3Connection = struct {
    io: Stream,

    pub fn get_socket(alloc: mem.Allocator) ![]const u8 {
        var o = try std.ChildProcess.exec(.{
            .allocator = alloc,
            .argv = &I3_GETSOCKET_CMD,
        });
        //return mem.trimRight(u8, o.stdout, "\n");
        //log.debug("{s}", .{o.stdout});
        return mem.trimRight(u8, o.stdout, "\n");
    }

    pub fn init(alloc: mem.Allocator) !I3Connection {
        const sock = try @This().get_socket(alloc);
        const io = try net.connectUnixSocket(sock);
        return I3Connection{
            .io = io,
        };
    }

    fn write(self: @This(), x: []const u8) !u64 {
        return try self.io.writer().write(x);
    }

    fn read(self: @This(), alloc: mem.Allocator, n: u64) ![]u8 {
        var b = try alloc.alloc(u8, n);
        var nb = try self.io.reader().read(b);
        if (nb == n) {
            return b;
        } else {
            //return I3Error.InvalidResponsePayload;
            return b;
        }
    }

    pub fn sendmsg(self: @This(), mtype: u32, msg: []const u8) !u64 {
        var nb: u64 = 0;
        nb += try self.write(I3_MAGIC_BYTES);
        nb += try self.write(mem.asBytes(@ptrCast(*const u32, &msg.len)));
        nb += try self.write(mem.asBytes(&mtype));
        nb += try self.write(msg);
        return nb;
    }

    //TODO: this returns raw bytes and doesn't parse input
    // would be nice to finish this, but don't think it's needed for this project
    pub fn recvmsg_raw(self: @This(), alloc: mem.Allocator) ![]u8 {
        const magic = try self.read(alloc, 6);
        if (!mem.eql(u8, magic, I3_MAGIC_BYTES)) return I3Error.InvalidResponsePayload;

        const nb = mem.readIntSliceNative(u32, try self.read(alloc, 4));

        _ = mem.readIntSliceNative(u32, try self.read(alloc, 4));

        return try self.read(alloc, nb);
    }

    pub fn sendrcv_raw(self: @This(), alloc: mem.Allocator, mtype: u32, msg: []const u8) ![]u8 {
        _ = try self.sendmsg(mtype, msg);
        return try self.recvmsg_raw(alloc);
    }

    pub fn cmd_raw(self: @This(), alloc: mem.Allocator, cmdstr: []const u8) ![]u8 {
        return self.sendrcv_raw(alloc, 0, cmdstr);
    }

    pub fn workspaces_raw(self: @This(), alloc: mem.Allocator) ![]u8 {
        return self.sendrcv_raw(alloc, 1, "");
    }

    //FIX: can't get this to parse for some reason...
    pub fn workspaces(self: @This(), alloc: mem.Allocator) ![]Workspace {
        const wsr = try self.workspaces_raw(alloc);
        const wsr1 = try mem.concat(alloc, u8, &[_][]const u8{ "{\"workspaces\":", wsr, "}" });
        var st = json.TokenStream.init(&wsr1);
        return try json.parse([]Workspace, &st, .{});
    }

    pub fn has_workspace(self: @This(), alloc: mem.Allocator, name: []const u8) !bool {
        var p = json.Parser.init(alloc, false);
        defer p.deinit();
        const s = try self.workspaces_raw(alloc);
        var t = try p.parse(s);
        for (t.root.Array.items) |ws| {
            const w = switch (ws) {
                .Object => |o| o.get("name"),
                else => continue,
            };
            const ww = w orelse continue;
            if (mem.eql(u8, ww.String, name)) return true;
        }
        return false;
    }
};

test "i3" {
    const alloc = std.heap.page_allocator;

    const cnxn = try I3Connection.init(alloc);

    const h = cnxn.has_workspace(alloc, "video");

    std.log.warn("{b}", .{h});
}
